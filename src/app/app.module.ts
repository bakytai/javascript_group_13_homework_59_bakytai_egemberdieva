import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RiddleComponent } from './riddle/riddle.component';
import { AnswerComponent } from './riddle/answer/answer.component';
import { QuestionsService } from "./shared/questions.service";
import { FormsModule } from "@angular/forms";
import { ChangeClassDirective } from './change-class.directive';

@NgModule({
  declarations: [
    AppComponent,
    RiddleComponent,
    AnswerComponent,
    ChangeClassDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [QuestionsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
