import {Questions} from "./questions.model";

export class QuestionsService  {
    public quesArray: Questions[] = [
      new Questions('Летит, а не птица, Воет, а не зверь.', 'Ветер', 'Ветер'),
      new Questions('Без рук, а рисует,Без зубов, а кусает.', 'Мороз', 'Мороз'),
      new Questions('isNaN(NaN): true or false?', 'true', 'true'),
      new Questions('isNaN(null): true or false?', 'false', 'false'),
    ]

  getQues() {
    return this.quesArray.slice();
  }

  checkUserAnswer(question: Questions, answer: string) {
      if (question.answer === answer) {
        question.status = 'correct';
      } else {
        question.status = 'incorrect';
      }
  }
}
