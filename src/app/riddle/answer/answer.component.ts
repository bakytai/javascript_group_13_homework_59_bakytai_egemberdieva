import { Component, ElementRef, Input,ViewChild } from '@angular/core';
import { QuestionsService } from "../../shared/questions.service";
import { Questions } from "../../shared/questions.model";


@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent {
  @Input() question!: Questions;

  @ViewChild('answerInput') answerInput!: ElementRef;

  constructor(private quesService: QuestionsService) { }

  checkUserAnswer() {
    const usersValue = this.answerInput.nativeElement.value;
    this.quesService.checkUserAnswer(this.question, usersValue);
  }
}
