import {Component,OnInit} from '@angular/core';
import {Questions} from "../shared/questions.model";
import {QuestionsService} from "../shared/questions.service";

@Component({
  selector: 'app-riddle',
  templateUrl: './riddle.component.html',
  styleUrls: ['./riddle.component.css']
})
export class RiddleComponent  implements OnInit{
  questions!: Questions[];
  show = false;

  constructor(private questionsService: QuestionsService) {}

  ngOnInit() {
    this.questions = this.questionsService.getQues();
  }

  showPrompt() {
    this.show = !this.show;
  }

  getCheck() {
    return this.questions.filter(q => q.status === 'correct').length
  }
}

